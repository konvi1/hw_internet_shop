package http

import (
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/konvi1/hw_internet_shop/internal/domain"
)

func dtoToItem(t ItemToCreate) (*domain.Item, error) {

	/*	if t.Title == nil {
			return nil, errors.New("invalid title")
		}
	*/

	id := domain.NextItemID()

	dt := &domain.Item{
		ID:    id,
		Title: t.Title, // *t.Title,
	}

	return dt, nil
}

func itemToDTO(dt *domain.Item) Item {
	var itemId *Id
	if dt.ID != "" {
		itemId = (*Id)(&dt.ID)
	}

	var title *string
	if dt.Title != "" {
		title = &dt.Title
	}

	return Item{
		ItemId: itemId,
		Title:  title,
	}
}

type APIErr struct {
	Message string `json:"message"`
}

func writeErr(w http.ResponseWriter, msg string) {
	if err := respondWithJSON(w, http.StatusNotFound, APIErr{Message: msg}); err != nil {
		log.Println(err)
	}
}

func writeOK(w http.ResponseWriter, payload interface{}) {
	if err := respondWithJSON(w, http.StatusOK, payload); err != nil {
		log.Println(err)
	}
}

func writeCreated(w http.ResponseWriter, payload interface{}) {
	if err := respondWithJSON(w, http.StatusCreated, payload); err != nil {
		log.Println(err)
	}
}

func respondWithJSON(w http.ResponseWriter, status int, payload interface{}) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	return json.NewEncoder(w).Encode(payload)
}
