package domain

import "github.com/google/uuid"

type ItemID string

type Item struct {
	ID    ItemID `json:"id"`
	Title string `json:"title"`
	Qty   int    `json:"qty"`
}

func NextItemID() ItemID {
	return ItemID(uuid.NewString())
}
