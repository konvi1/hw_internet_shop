package http

import (
	"net/http"

	"github.com/go-chi/chi/v5"
)

func NewServer(addr string, ctrl *Controller) *http.Server {
	router := chi.NewRouter()

	HandlerFromMux(ctrl, router)

	return &http.Server{
		Addr:    addr,
		Handler: router,
	}
}
