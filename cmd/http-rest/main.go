package main

import (
	"context"
	"flag"
	"log"
	"net/http"
	"os/signal"
	"syscall"

	//"time"

	//"github.com/patrickmn/go-cache"
	"gitlab.com/konvi1/hw_internet_shop/internal/domain"
	"gitlab.com/konvi1/hw_internet_shop/internal/storage"

	httpAPI "gitlab.com/konvi1/hw_internet_shop/internal/api/http"
	"golang.org/x/sync/errgroup"
)

func main() {
	addr := flag.String("addr", "http://127.0.0.1:8080", "http server addr")
	flag.Parse()

	ctx := context.Background()
	ctx, cancel := signal.NotifyContext(ctx,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)
	defer cancel()

	//ch := cache.New(1*time.Hour, 1*time.Hour)
	st := storage.NewItemStorage()
	fillStorage(st)
	ctrl := httpAPI.NewController(st)
	srv := httpAPI.NewServer(*addr, ctrl)

	g, ctx := errgroup.WithContext(ctx)
	g.Go(func() error {
		if er := srv.ListenAndServe(); er != nil && er != http.ErrServerClosed {
			log.Fatal("ListenAndServe err", er)
		}
		return nil
	})
	g.Go(func() error {
		<-ctx.Done()
		if er := srv.Shutdown(ctx); er != nil {
			log.Fatal("Shutdown err", er)
		}
		return nil
	})
	log.Println("server listen", *addr)
	if err := g.Wait(); err != nil {
		log.Fatal("Wait server err", err)
	}
	log.Println("server stop")
}

func fillStorage(st storage.ItemStorage) {
	st.StoreItem(&domain.Item{
		ID:    "b9ffcf94-e020-4879-a43e-5b6b41309678",
		Title: "ABC",
	})
	st.StoreItem(&domain.Item{
		ID:    "erffcf94-e020-4879-a43e-5b6b41309678",
		Title: "dd",
	})
	log.Print(st)
}
