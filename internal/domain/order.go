package domain

import "github.com/google/uuid"

type OrderID string

type Order struct {
	ID OrderID `json:"id"`
	//	Title string `json:"title"`
	Status  string `json:"status"`
	Address string `json:"address"`
}

func NextOrderID() OrderID {
	return OrderID(uuid.NewString())
}
