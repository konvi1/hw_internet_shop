package http

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"gitlab.com/konvi1/hw_internet_shop/internal/domain"
	"gitlab.com/konvi1/hw_internet_shop/internal/storage"
)

func NewController(s storage.ItemStorage) *Controller {
	return &Controller{s: s}
}

type Controller struct {
	ServerInterface
	s storage.ItemStorage
}

func (c *Controller) GetItemsItemId(w http.ResponseWriter, r *http.Request, itemId ItemId) {
	fmt.Println("GetItemsItemId", itemId)

	itemID := domain.ItemID(itemId)

	t, found := c.s.GetItemByID(itemID)
	if !found {
		writeErr(w, fmt.Sprintf("not found item by id %v", itemId))
		return
	}

	writeOK(w, itemToDTO(t))
}

func (c *Controller) PostItems(w http.ResponseWriter, r *http.Request) {
	fmt.Println("PostItems")

	b, err := io.ReadAll(r.Body)
	if err != nil {
		writeErr(w, fmt.Sprintf("request error %v", err))
		return
	}
	defer r.Body.Close()

	var req PostItemsJSONRequestBody
	if err := json.Unmarshal(b, &req); err != nil {
		writeErr(w, fmt.Sprintf("request error %v", err))
		return
	}

	t, err := dtoToItem(ItemToCreate(req))
	if err != nil {
		writeErr(w, fmt.Sprintf("request error %v", err))
		return
	}

	c.s.StoreItem(t)

	writeCreated(w, itemToDTO(t))
}
