package storage

import (
	"log"

	"gitlab.com/konvi1/hw_internet_shop/internal/domain"
)

// DB

var (
	// слайс товаров
	ItemStorageSlice []domain.Item
)

type ItemStorage interface {
	StoreItem(*domain.Item)
	GetItemByID(domain.ItemID) (*domain.Item, bool)
}

func NewItemStorage() ItemStorage {
	return &itemStorage{ItemStorageSlice: []domain.Item{}}
}

// структура с товарами
type itemStorage struct {
	ItemStorageSlice []domain.Item
}

// добавляем товар в поле ItemStorageSlice структуры itemStorage
func (s *itemStorage) StoreItem(t *domain.Item) {
	_, ok := s.FindItemByID(t.ID)
	if ok {
		log.Println("Exist ID:", t.ID)
		return
	}
	s.ItemStorageSlice = append(s.ItemStorageSlice, *t)
	log.Println("Add item with ID 5: ", t.ID)
	log.Println("ItemStorageSlice: ", s.ItemStorageSlice)

}

// получаем товар из поля ItemStorageSlice структуры itemStorage
func (s *itemStorage) GetItemByID(id domain.ItemID) (*domain.Item, bool) {
	return s.FindItemByID(id)

}

// поиск по id из поля ItemStorageSlice структуры itemStorage
func (s *itemStorage) FindItemByID(id domain.ItemID) (*domain.Item, bool) {
	for _, v := range s.ItemStorageSlice {
		if v.ID == id {
			return &v, true
		}
	}

	return nil, false
}
