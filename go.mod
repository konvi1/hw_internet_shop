module gitlab.com/konvi1/hw_internet_shop

go 1.18

require (
	github.com/deepmap/oapi-codegen v1.10.1
	github.com/go-chi/chi/v5 v5.0.7
	github.com/google/uuid v1.3.0
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
)
